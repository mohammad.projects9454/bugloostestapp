<?php

use App\Http\Controllers\InsertJsonController;
use App\Http\Controllers\InsertXmlController;
use App\Http\Controllers\JsonController;
use App\Http\Controllers\XmlController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ---------- Home Path ---------- \\
Route::get('/', function () {
    return view('welcome');
})->name('home');

// ---------- Path for fetch data from api and show in table using below controllers ---------- \\
// ---------- support both xml and json ---------- \\
Route::get('/xml', XmlController::class)->name('xmlResponse');
Route::get('/json', JsonController::class)->name('jsonResponse');

// ---------- Path for store data to DB and turn message back to home page using below controllers ---------- \\
// ---------- support both xml and json ---------- \\
Route::post('/jsonInsert', [InsertJsonController::class, 'storeJson']);
Route::post('/xmlInsert', [InsertXmlController::class, 'storeXml']);
