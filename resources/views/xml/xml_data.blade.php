<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bugloos</title>
    <link rel="stylesheet" href="{{ asset('style.css') }}">
</head>

<body style="padding-top: 3%; padding-bottom: 3%; padding-left: 1%; padding-right: 1%;">
    <form action="/xmlInsert" method="POST">
        @csrf
        <input type="hidden" value="{{ json_encode($xmlValue) }}" name="xml">
        <button class="submitButton" type="submit">ثبت در پایگاه داده</button>
    </form>
    <h1 class="jsonFetchH1" dir="rtl">داده دریافت شده در قالب Xml:</h1>
    <a class="back" href="/">برگشت</a>
    <table>
        <tr>
            <th>id</th>
            <th>title</th>
            <th>price</th>
            <th>description</th>
            <th>category</th>
            <th>image</th>
            <th>rate</th>
            <th>count</th>
        </tr>
        @foreach ($xmlValue as $xValue)
        <tr>
            <td>{{ $xValue->id }}</td>
            <td>{{ $xValue->title }}</td>
            <td>{{ $xValue->price }}</td>
            <td>{{ $xValue->description }}</td>
            <td>{{ $xValue->category }}</td>
            <td><img src="{{ $xValue->image }}" width="100px" height="150px" /></td>
            <td>{{ $xValue->rating->rate }}</td>
            <td>{{ $xValue->rating->count }}</td>
        </tr>
        @endforeach
    </table>
</body>

</html>