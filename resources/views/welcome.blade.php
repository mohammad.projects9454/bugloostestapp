<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bugloos</title>

    <link rel="stylesheet" href="{{ asset('style.css') }}" />
</head>

<body style="overflow: hidden;">
    @if (session('status'))
    <script type="text/javascript">
        alert('<?php echo session('status'); ?>');
    </script>
    @endif

    <div class="btn-group">
        <a href="{{ route('jsonResponse') }}" dir="rtl"><span>دریافت JSON</span></a>
        <a href="{{ route('xmlResponse') }}" dir="rtl"><span>دریافت XML</span></a>
    </div>
</body>

</html>