<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\InsertRepositoryInterface;
use Illuminate\Http\Request;

// ---------- Controller for insert xml data to DB ---------- \\
class InsertXmlController extends Controller
{
    // ---------- Define InsertRepositoryInterface object to comply SOLID Principles and Repository pattern  ---------- \\
    private InsertRepositoryInterface $insertXmlRepository;

    // ---------- Constructor of this controller for initialize repositry object  ---------- \\
    public function __construct(InsertRepositoryInterface $insertXmlRepository)
    {
        $this->insertXmlRepository = $insertXmlRepository;
    }

    // ---------- Object called interface method for store data to DB and redirect ---------- \\
    public function storeXml(Request $request)
    {
        $this->insertXmlRepository->save($request);
        return redirect()->route('home')->with("status", "اطلاعات ثبت شد");
    }
}
