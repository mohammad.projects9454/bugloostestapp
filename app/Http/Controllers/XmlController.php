<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\FormatRepositoryInterface;
use Illuminate\Http\Request;

// ---------- Controller for Print xml data from api ---------- \\
class XmlController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // ---------- Define FormatRepositoryInterface object to comply SOLID Principles and Repository pattern  ---------- \\
    private FormatRepositoryInterface $xmlRepository;

    // ---------- Constructor of this controller for initialize repositry object  ---------- \\
    public function __construct(FormatRepositoryInterface $xmlRepository)
    {
        $this->xmlRepository = $xmlRepository;
    }

    // ---------- This method is only method of Single Action Controller ---------- \\
    // ---------- Object called interface method for fetch data from specified url and pass to blade file ---------- \\
    public function __invoke(Request $request)
    {
        $xmlValue = $this->xmlRepository->print("https://mohammadaligholian.ir/fakexml.php");
        return view('xml.xml_data', ['xmlValue' => $xmlValue]);
    }
}
