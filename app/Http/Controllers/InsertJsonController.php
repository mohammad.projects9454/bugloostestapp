<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\InsertRepositoryInterface;
use Illuminate\Http\Request;

// ---------- Controller for insert json data to DB ---------- \\
class InsertJsonController extends Controller
{
    // ---------- Define InsertRepositoryInterface object to comply SOLID Principles and Repository pattern  ---------- \\
    private InsertRepositoryInterface $insertJsonRepository;

    // ---------- Constructor of this controller for initialize repositry object  ---------- \\
    public function __construct(InsertRepositoryInterface $insertJsonRepository)
    {
        $this->insertJsonRepository = $insertJsonRepository;
    }

    // ---------- Object called interface method for store data to DB and redirect ---------- \\
    public function storeJson(Request $request)
    {
        $this->insertJsonRepository->save($request);
        return redirect()->route('home')->with("status", "اطلاعات ثبت شد");
    }
}
