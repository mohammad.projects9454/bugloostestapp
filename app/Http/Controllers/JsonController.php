<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\FormatRepositoryInterface;
use Illuminate\Http\Request;

// ---------- Controller for Print json data from api ---------- \\
class JsonController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // ---------- Define FormatRepositoryInterface object to comply SOLID Principles and Repository pattern  ---------- \\
    private FormatRepositoryInterface $jsonRepository;

    // ---------- Constructor of this controller for initialize repositry object  ---------- \\
    public function __construct(FormatRepositoryInterface $jsonRepository)
    {
        $this->jsonRepository = $jsonRepository;
    }

    // ---------- This method is only method of Single Action Controller ---------- \\
    // ---------- Object called interface method for fetch data from specified url and pass to blade file ---------- \\
    public function __invoke(Request $request)
    {
        $jsonValue = $this->jsonRepository->print("https://mohammadaligholian.ir/fakejson.php");
        return view('json.json_data', ['jsonValue' => $jsonValue]);
    }
}
