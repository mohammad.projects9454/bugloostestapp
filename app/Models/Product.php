<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'price', 'description', 'category', 'image', 'rate', 'count'];

    // ---------- Mutator for convert value to custom value ---------- \\
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = round((float)$value);
    }

    // ---------- Mutator for convert value to custom value ---------- \\
    public function setRateAttribute($value)
    {
        $this->attributes['rate'] = $value . ' stars';
    }

    // ---------- Mutator for convert value to custom value ---------- \\
    public function setCountAttribute($value)
    {
        if ((((int) $value)) < 150)
            $value = "finishing: " . $value;
        $this->attributes['count'] = $value;
    }
}
