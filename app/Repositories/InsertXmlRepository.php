<?php

namespace App\Repositories;

use App\Repositories\Interfaces\InsertRepositoryInterface;
use App\Models\Product;

// ---------- this class is a repository class that implement InsertRepositoryInterface to achieve save method for only xml ---------- \\
class InsertXmlRepository implements InsertRepositoryInterface
{
    // ---------- this method stroe data to DB ---------- \\
    public function save($request)
    {
        $jArray = json_decode($request->input("xml"));

        for ($i = 0; $i < count($jArray->row); $i++) {
            $product = new Product();
            $product->price = $jArray->row[$i]->price;
            $product->title = $jArray->row[$i]->title;
            $product->description = $jArray->row[$i]->description;
            $product->category = $jArray->row[$i]->category;
            $product->image = $jArray->row[$i]->image;
            $product->rate = $jArray->row[$i]->rating->rate;
            $product->count = $jArray->row[$i]->rating->count;

            $product->save();
        }
    }
}
