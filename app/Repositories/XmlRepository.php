<?php

namespace App\Repositories;

use App\Repositories\Interfaces\FormatRepositoryInterface;

// ---------- this class is a repository class that implement FormatRepositoryInterface to achieve print method for only xml ---------- \\
class XmlRepository implements FormatRepositoryInterface
{
    // ---------- this method fetch data and transfer it to controller ---------- \\
    public function print($url)
    {
        $xml = file_get_contents($url);
        $xmlData = simplexml_load_string($xml) or die("Error: Cannot create object");
        return $xmlData;
    }
}
