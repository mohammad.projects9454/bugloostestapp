<?php

namespace App\Repositories;

use App\Repositories\Interfaces\FormatRepositoryInterface;

// ---------- this class is a repository class that implement FormatRepositoryInterface to achieve print method for only json ---------- \\
class JsonRepository implements FormatRepositoryInterface
{

    // ---------- this method fetch data and transfer it to controller ---------- \\
    public function print($url)
    {
        $json = json_decode(file_get_contents($url), true);

        return $json;
    }
}
