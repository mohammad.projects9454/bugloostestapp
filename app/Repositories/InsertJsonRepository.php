<?php

namespace App\Repositories;

use App\Repositories\Interfaces\InsertRepositoryInterface;
use App\Models\Product;

// ---------- this class is a repository class that implement InsertRepositoryInterface to achieve save method for only json ---------- \\
class InsertJsonRepository implements InsertRepositoryInterface
{
    // ---------- this method stroe data to DB ---------- \\
    public function save($request)
    {
        $jArray = json_decode($request->input("json"));


        foreach ($jArray as $arrItem) {

            $product = new Product();
            $product->price = $arrItem->price;
            $product->title = $arrItem->title;
            $product->description = $arrItem->description;
            $product->category = $arrItem->category;
            $product->image = $arrItem->image;
            $product->rate = $arrItem->rating->rate;
            $product->count = $arrItem->rating->count;

            $product->save();
        }
    }
}
