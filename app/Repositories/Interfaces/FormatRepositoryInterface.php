<?php

namespace App\Repositories\Interfaces;

// ---------- Interface that has a method print() for show data from api url ---------- \\
// ---------- Repository classes will implement this interface ---------- \\
interface FormatRepositoryInterface
{
    public function print(string $url);
}
