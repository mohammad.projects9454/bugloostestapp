<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;

// ---------- Interface that has a method save() for store data to DB ---------- \\
// ---------- Repository classes will implement this interface ---------- \\
interface InsertRepositoryInterface
{
    public function save(Request $request);
}
