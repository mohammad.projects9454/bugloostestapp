<?php

namespace App\Providers;

use App\Http\Controllers\InsertJsonController;
use App\Http\Controllers\InsertXmlController;
use App\Http\Controllers\JsonController;
use App\Http\Controllers\XmlController;
use App\Repositories\InsertJsonRepository;
use App\Repositories\InsertXmlRepository;
use App\Repositories\JsonRepository;
use App\Repositories\XmlRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\FormatRepositoryInterface;
use App\Repositories\Interfaces\InsertRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /**
         *  These lines are responsible for which repository class to create
         *  an instance of when we create an instance from the interface
         **/
        $this->app->when(XmlController::class)->needs(FormatRepositoryInterface::class)->give(function () {
            return new XmlRepository();
        });
        $this->app->when(JsonController::class)->needs(FormatRepositoryInterface::class)->give(function () {
            return new JsonRepository();
        });
        $this->app->when(InsertJsonController::class)->needs(InsertRepositoryInterface::class)->give(function () {
            return new InsertJsonRepository();
        });
        $this->app->when(InsertXmlController::class)->needs(InsertRepositoryInterface::class)->give(function () {
            return new InsertXmlRepository();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
